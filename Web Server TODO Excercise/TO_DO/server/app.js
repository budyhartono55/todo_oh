const express = require("express");
const bodyParser = require("body-parser");
const logger = require("morgan");

//routes

//dataBase

//port and get express
const app = express();
const PORT = 8888;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

require("./routes/todoRoutes.js")(app);

app.use(logger("dev"));

app.get("/", (req, res) => res.send("Hello From TODO"));

app.listen(PORT, () => {
  console.log(`Server running on port : http://localhost:${PORT}/todo, lets see...`);
});
