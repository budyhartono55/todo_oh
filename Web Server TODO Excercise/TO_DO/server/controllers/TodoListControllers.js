const TodoList = require("../models/TodoList");

const getData = async (req, res) => {
  let todoList = new TodoList();

  return res.status(200).send({
    status: "SUCCESS",
    data: await todoList.get(),
  });
};

const newData = (req, res) => {
  let todoList = new TodoList();

  todoList.add({
    name: req.body.name,
    date: req.body.date,
    note: req.body.note,
  });

  todoList.save();

  return res.status(200).send({
    status: "SUCCESS",
    data: todoList.get(),
  });
};

module.exports = {
  getData,
  newData,
};
