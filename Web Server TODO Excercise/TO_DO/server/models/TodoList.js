const fs = require("fs");

class TodoList {
  constructor() {
    this.todoList = [];
    this.read();
  }

  add(newTodo) {
    console.log(`before add ${JSON.stringify(this.todoList)}`);
    this.todoList.push(newTodo);
    console.log(`after add ${JSON.stringify(this.todoList)}`);
  }

  get() {
    return this.todoList;
  }

  read() {
    const data = fs.readFileSync("./models/data.json", "utf8");

    this.todoList = JSON.parse(data);

    return this.todoList;
    // return this.todoList;
  }

  async save() {
    fs.writeFile("./models/data.json", JSON.stringify(this.todoList), (err) => {
      if (err) throw err;
      console.log("Post data Success!");
    });
  }
}

module.exports = TodoList;
