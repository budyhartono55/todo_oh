const express = require("express");
const fs = require("fs");
const { v4 } = require("uuid");
const controller = require("../controllers/TodoListControllers");

let todos = [];

//router methode
//get
module.exports = function (app) {
  app.get("/todo", controller.getData);

  app.post("/todo", controller.newData);

  // //post
  // const post = router.post("/", (req, res) => {
  //   const todo = req.body;
  //   todos.push({ ...todo, id: v4() });

  //   res.send(`POST todo with the name ${todo.name} was COMPLETED added to the Database`);
  // });

  // //update with methode PATCH
  // const put = router.put("/:id", (req, res) => {
  //   const { id } = req.params;
  //   const { name, date, note } = req.body;

  //   const todo = todos.find((todo) => todo.id == id);

  //   if (name) {
  //     todo.name = name;
  //   }

  //   if (date) {
  //     todo.date = date;
  //   }

  //   if (note) {
  //     todo.note = note;
  //   }

  //   res.send(`Todo with id: <${id}> was UPDATED`);
  // });

  // //get by ID
  // const get = router.get("/:id", (req, res) => {
  //   const { id } = req.params;
  //   const findTodoById = todos.find((todo) => todo.id == id);

  //   res.send(findTodoById);
  // });

  // router.delete("/:id", (req, res) => {
  //   const { id } = req.params;
  //   todos = todos.filter((todo) => todo.id !== id);

  //   res.send(`Todo with id: <${id}> was DELETED`);
  // });
};
